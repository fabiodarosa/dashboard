import React from 'react';
import jwt_decode from "jwt-decode";
import axios from 'axios'


type IdInfo = {
    name: string
    preferred_username: string
}

function App() {
    const urlParams = new URLSearchParams(window.location.search)

    const id = urlParams.get("id_token") || ""
    let data = ""
    if (id !== "")
        data = jwt_decode<IdInfo>(id).name

    const logout = () => {
        axios.get("https://d0e9-2804-7f4-c2a3-3ce2-e9a4-cd4a-db9d-9572.ngrok.io/logout")
    }

    return (<>
        <h1>Bem vindo {data}</h1>

        <button onClick={logout}></button>
    </>);


}

export default App;
